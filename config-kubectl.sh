# Run this directly from the web with:
# If known, add cluster name at the end of the string, providing a cluster name presumes it is in the region CloudShell is running

# s=config-kubectl.sh ; curl -sSL https://gitlab.com/guided-explorations/aws/aws-cloudshell-configs/-/raw/main/${s} -o /tmp/${s}; chmod +x /tmp/${s}; bash /tmp/${s}

echo "AWS Cloudshell Configuration"

if [[ -n "$1" ]]; then
  CLUSTERNAME=$1
  echo "Configuring cluster provided on command line: ${CLUSTERNAME}"
else
  read -e -p "What region should I enumerate EKS clusters in? " -i ${AWS_DEFAULT_REGION} TARGETREGION
  echo "Listing clusters in this region (${TARGETREGION}):"
  aws eks list-clusters --region ${TARGETREGION} --output "text"

  read -p "Please enter the EKS cluster name from above: " CLUSTERNAME
fi

aws eks update-kubeconfig --region ${TARGETREGION} --name ${CLUSTERNAME}
