
# Run this directly from the web with:
# s=add-nano.sh; curl -sSL https://gitlab.com/guided-explorations/aws/aws-cloudshell-configs/-/raw/main/${s} -o /tmp/${s}; chmod +x /tmp/${s}; bash /tmp/${s}

echo "AWS Cloudshell Configuration"
if [[ -z $(command -v nano) ]]; then
  echo "adding Nano editor"
  sudo yum install -y nano
fi
echo "NANO VERSION: $(nano --version)"
