# AWS CloudShell Run From Web Configuration Scripts

## Getting started

The scripts here help configure [AWS Cloud Shell](https://docs.aws.amazon.com/cloudshell/latest/userguide/welcome.html) for your purposes.

The the why and how blog article backing this code is here: [Mission Impossible Code: AWS CloudShell "Run From Web" Configuration Scripts](https://missionimpossiblecode.io/aws-cloudshell-run-from-web-configuration-scripts)

They can be run directly from this repository with a command line:

`s=add-all.sh; curl -sSL https://gitlab.com/guided-explorations/aws/aws-cloudshell-configs/-/raw/main/${s} -o /tmp/${s}; chmod +x /tmp/${s}; bash /tmp/${s}`

The above one does the "add-all.sh" script, but can be used with any script. Each script has it's run from web command in a comment at the top.

Saving and running scripts locally (and not piping directly to bash) is important for using things like 'read' as well as other edge cases.

Aggregating scripts, like add-all.sh can simply call the oneliner of other scripts.

Changes to installs only persist during the session. Data in $HOME persists for 120 days and is per-account / per-region specific.