# Run this directly from the web with:
# s=add-helm.sh; curl -sSL https://gitlab.com/guided-explorations/aws/aws-cloudshell-configs/-/raw/main/${s} -o /tmp/${s}; chmod +x /tmp/${s}; bash /tmp/${s}

echo "AWS Cloudshell Configuration"
if [[ -z $(command -v helm) ]]; then
  echo "Adding helm"
  export VERIFY_CHECKSUM=false; 
  curl -sSL https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash
  sudo ln /usr/local/bin/helm /usr/bin/helm
fi
echo "HELM VERSION: $(helm version --short)"