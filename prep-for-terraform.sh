
# Commands for AWS CloudShell that help ensure you have enough room for a full download of dependencies
# but that state is stored in persistent storage

# Run this directly from the web with:
# s=prep-for-terraform.sh ; curl -sSL https://gitlab.com/guided-explorations/aws/aws-cloudshell-configs/-/raw/main/${s} -o /tmp/${s}; chmod +x /tmp/${s}; bash /tmp/${s}


TERRAFORMDIR=/terraform
TERRAFORMSTATEDIR=$HOME/tfstate
echo "Terraform command directory is ${TERRAFORMDIR} and state will be stored in $HOME/tfstate"

if [[ ! -d ${TERRAFORMDIR} ]]; then 
  echo "One time setup for ${TERRAFORMDIR}"
  sudo mkdir -p  ${TERRAFORMDIR}
  sudo chown $USER:root ${TERRAFORMDIR}
  sudo chmod 775 ${TERRAFORMDIR}
fi

if [[ -d ${TERRAFORMDIR} ]]; then echo "The setup is complete for: ${TERRAFORMDIR}"; fi

if [[ ! -d ${TERRAFORMSTATEDIR} ]]; then mkdir -p ${TERRAFORMSTATEDIR}; fi
cd ${TERRAFORMDIR}
#git clone or other acquisition of terraform template

# sample terraform commands to deploy https://github.com/aws-ia/terraform-aws-eks-blueprints/tree/main/examples/karpenter
# cd /terraform
# git clone https://github.com/aws-ia/terraform-aws-eks-blueprints.git
# cd terraform-aws-eks-blueprints/examples/karpenter
# terraform init

# When your terminal times out, your /terraform ($TERAFORMDIR) will disappear.
# To keep working run this script again and then turn the four above commands to reestablish the terraform template
# and then you can continue with more commands, for example the teardown commands.

# Deploy commands from: https://github.com/aws-ia/terraform-aws-eks-blueprints/blob/main/examples/karpenter/README.md#user-content-deploy
# terraform apply -target module.vpc -state=$HOME/tfstate/eksblueprint.tfstate
# terraform apply -target module.eks -state=$HOME/tfstate/eksblueprint.tfstate
# terraform apply -state=$HOME/tfstate/eksblueprint.tfstate
#
# If your terminal times out, your /terraform folder will disappear. When that happens rerun this script
#
# Teardown commands from: https://github.com/aws-ia/terraform-aws-eks-blueprints/blob/main/examples/karpenter/README.md#user-content-destroy
# kubectl delete deployment inflate
# terraform destroy -target="module.eks_blueprints_kubernetes_addons" -auto-approve -state=$HOME/tfstate/eksblueprint.tfstate
# terraform destroy -target="module.eks" -auto-approve -state=$HOME/tfstate/eksblueprint.tfstate
# terraform destroy -auto-approve -state=$HOME/tfstate/eksblueprint.tfstate
#