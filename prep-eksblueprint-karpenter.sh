# Run this directly from the web with:
# s=prep-eksblueprint-karpenter.sh ; curl -sSL https://gitlab.com/guided-explorations/aws/aws-cloudshell-configs/-/raw/main/${s} -o /tmp/${s}; chmod +x /tmp/${s}; bash /tmp/${s}

s=add-all.sh ; curl -sSL https://gitlab.com/guided-explorations/aws/aws-cloudshell-configs/-/raw/main/${s} -o /tmp/${s}; chmod +x /tmp/${s}; bash /tmp/${s}

s=prep-for-terraform.sh ; curl -sSL https://gitlab.com/guided-explorations/aws/aws-cloudshell-configs/-/raw/main/${s} -o /tmp/${s}; chmod +x /tmp/${s}; bash /tmp/${s}

cd /terraform
if [[ ! -d /terraform/terraform-aws-eks-blueprints ]]; then
  git clone https://github.com/aws-ia/terraform-aws-eks-blueprints.git --no-checkout /terraform/terraform-aws-eks-blueprints
  cd /terraform/terraform-aws-eks-blueprints/
  git reset --hard tags/v4.29.0
  git clone https://gitlab.com/guided-explorations/aws/eks-runner-configs/gitlab-runner-eks-fargate.git /terraform/terraform-aws-eks-blueprints/examples/glrunner
fi
cd /terraform/terraform-aws-eks-blueprints/examples/glrunner

echo "Karpenter EKS Blueprint ready for customization and 'terraform init' in /terraform/terraform-aws-eks-blueprints/examples/glrunner use 'cd /terraform/terraform-aws-eks-blueprints/examples/glrunner'"
