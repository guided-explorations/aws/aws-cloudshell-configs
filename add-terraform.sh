
# Run this directly from the web with:
# s=add-terraform.sh; curl -sSL https://gitlab.com/guided-explorations/aws/aws-cloudshell-configs/-/raw/main/${s} -o /tmp/${s}; chmod +x /tmp/${s}; bash /tmp/${s}

echo "AWS Cloudshell Configuration"

if [[ -z $(command -v terraform) ]]; then
  echo "adding terraform"
  sudo yum install -y yum-utils 
  sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/AmazonLinux/hashicorp.repo
  sudo yum -y install terraform
fi 
echo "TERRAFORM VERSION: $(terraform --version)"